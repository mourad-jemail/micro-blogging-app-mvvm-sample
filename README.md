# Android MVVM architecture: Micro Blogging Sample App

The idea is to create a client app for a micro blogging platform. The app implements MVVM architecture using the Android Architecture Components (AKA Jetpack libraries) and other common libraries.

---

### This app is composed of 4 screens:

1. The first screen is displayed on entry and shows a list of authors.	
![1st screen](https://media1.tenor.com/images/35f96a6544f97cd4fb6faec700213976/tenor.gif?itemid=19328795)

2. When user clicks an author, he is taken to the second screen. This UI shows a header with details about the author and a listing of all the posts written by this author. The list of posts is ordered by date of creation, newest on the top, oldest on the bottom.		
![2nd screen](https://media1.tenor.com/images/e0ab9ef4d40d7075ce0d46511d900595/tenor.gif?itemid=19328796)

3. When user clicks a post, he is redirected to the third screen. This UI shows a header with details about the post and the number of comments about it.	
![3rd screen](https://media1.tenor.com/images/e483f0a55be71a3db037a9df378f5f1e/tenor.gif?itemid=19328798)

4. Finally, when user clicks on the number of comments, he is taken to the fourth screen. This UI shows a list of comments about that post, which is ordered by date of creation, newest on the top, oldest on the bottom.	
![4th screen](https://media1.tenor.com/images/0b9816dad6108bbefad83b711b25f9ea/tenor.gif?itemid=19328802)

---

### What is MVVM ?

1. **Model:** It represents the actual data and/or information the application is dealing with.
In an Android app, the Model also holds classes which **operate on the app’s data and fetch it from the local database or from the network**. While data is exposed to ViewModels through observables, Google recommends to do it via a Repository. **A Repository** is part of the Model in MVVM. It's placed as an intermediate module between data and ViewModels and **is considered as the single source of truth for ViewModels**. It plays the role of mediator between different data sources and checks whether the remote data should be cached locally.

2. **View:** It represents the UI of the application (Activity/Fragment). Views **handle only interactions with the user and don't hold any Business Logic**. In MVVM, the View is active. Meaning, they react to Model changes.

3. **ViewModel:** It acts as a link between the Model and the View. ViewModels provide data for Views by getting it from the Repository. In order to do so, ViewModels **don't hold references of Views. Instead, they expose data through observables**. ViewModels also, **help maintain the state of the View** and **expose functions that help manipulate the Model** as a result of user interactions with the View.
![MVVM](https://media1.tenor.com/images/1c5c4704fe25ad4e7073c43378391dec/tenor.gif?itemid=19331895)

---

### Why MVVM ?

1. It empowers the separation of concerns principle.

2. It separates View (i.e. Activities and Fragments) from Business Logic: most of the View logic is actually centered in the ViewModel.

3. In this architecture, the children don't have direct reference to the parent. More particularly, only View holds the reference to ViewModel and not vice versa (as opposed to MVP architecture), which means more loose coupling.

4. It is fully supported and encouraged by Google with their first-party libraries: Android Architecture Components provide built-in ViewModel, LiveData classes which are lifecycle-aware.

---

### This app does things like:
- getting data of authors, post and comments from an API using Retrofit and Gson,
- caching this data in an SQLite database using Room library,
- handling asynchronous code using Kotlin Coroutines,
- concentrating data operations in a Repository class,
- injecting dependencies using Kodein library,
- presenting fragments and the connections between them using Navigation component,
- displaying and managing complex RecyclerView layouts using Groupie library,
- loading and caching images using Glide library, ...etc.

---

### This app has the following packages:

1. **data:** it represents the entities and the business logic of the Android application (local and remote data source, model classes and repository).

2. **internal:** utility classes which expose custom features or exceptions that can be invoked in any other class of the app.

3. **ui:** Activity/Fragment classes along with their corresponding ViewModels.		
![project structure](https://media1.tenor.com/images/a44ff415f68f8ad8a20e95bd3efe9b9d/tenor.gif?itemid=19330802)

---

### In this project, I tried to focus on the following principles:

1. High code quality and re-usability.

2. Modular code and future proof (easily modifiable to add more features in the future if the platform evolves).

3. Proper use of design patterns:
	- **Creational patterns:** singleton, dependency injection, ...
	- **Structural patterns:** adapter, facade (i.e. the repository class), ...
	- **Behavioral patterns:** observer (i.e. LiveData class) , ...etc

4. Comments to explain some complex syntax / instructions in code.

5. Granular commits and epics / features are self-explanatory about the approach (UI is driven from Model) and the steps followed while developing this app.

---

### References:

- The demo app is available on Google Play [here](https://play.google.com/store/apps/details?id=com.demo.microbloggingapp).
- The API endpoints are described [here](https://sym-json-server.herokuapp.com).
- Approach and best practices are inspired from [Google guides](https://developer.android.com/jetpack/guide) and [ResoCoder](https://resocoder.com).
- Other references: [1](https://www.youtube.com/playlist?list=PLWz5rJ2EKKc9mxIBd0DRw9gwXuQshgmn2), [2](https://www.journaldev.com/20292/android-mvvm-design-pattern), [3](https://www.toptal.com/android/android-apps-mvvm-with-clean-architecture), [4](https://android.jlelse.eu/why-to-choose-mvvm-over-mvp-android-architecture-33c0f2de5516), [5](https://blog.mindorks.com/mvvm-architecture-android-tutorial-for-beginners-step-by-step-guide), [6](https://www.wintellect.com/model-view-viewmodel-mvvm-explained), [7](https://medium.com/hongbeomi-dev/create-android-app-with-mvvm-pattern-simply-using-android-architecture-component-529d983eaabe).

---

### Perspectives:

- More interactive UI's:
	- Possibility to refresh data,
	- In case of a connectivity failure, show a message, ...etc.
- Improve UX:
    - Add support for landscape layouts,
    - Integrate a paging feature for a better management of long lists, ...etc.
- Enhance the integration of [Clean Architecture](https://developer.android.com/jetpack/guide) practices.
- Demonstrate the testability of different functions in the app (unit testing).
