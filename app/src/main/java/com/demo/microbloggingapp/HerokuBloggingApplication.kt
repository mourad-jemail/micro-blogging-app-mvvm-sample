package com.demo.microbloggingapp

import android.app.Application
import com.demo.microbloggingapp.data.db.HerokuBloggingDatabase
import com.demo.microbloggingapp.data.network.ConnectivityInterceptor
import com.demo.microbloggingapp.data.network.ConnectivityInterceptorImpl
import com.demo.microbloggingapp.data.network.HerokuBloggingApiService
import com.demo.microbloggingapp.data.network.author.AuthorNetworkDataSource
import com.demo.microbloggingapp.data.network.author.AuthorNetworkDataSourceImpl
import com.demo.microbloggingapp.data.network.comment.CommentNetworkDataSource
import com.demo.microbloggingapp.data.network.comment.CommentNetworkDataSourceImpl
import com.demo.microbloggingapp.data.network.post.PostNetworkDataSource
import com.demo.microbloggingapp.data.network.post.PostNetworkDataSourceImpl
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepositoryImpl
import com.demo.microbloggingapp.ui.author.detail.AuthorViewModelFactory
import com.demo.microbloggingapp.ui.author.list.AuthorListViewModelFactory
import com.demo.microbloggingapp.ui.comment.CommentListViewModelFactory
import com.demo.microbloggingapp.ui.post.PostViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.*

class HerokuBloggingApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@HerokuBloggingApplication))

        //NOTE:
        // 1) We want to bind our database. However HerokuBloggingDatabase doesn't have any kind of
        // interface. So we're gonna bind the HerokuBloggingDatabase directly.
        // 2) We use singleton because it doesn't make sense to have multiple instances of the
        // database at the same time.
        // 3) We can use the invoke() operator to pass in the Context, like this:
        // bind() from singleton { HerokuBloggingDatabase.invoke(instance()) }
        // but we can omit the invoke() operator and call it simply like the instruction below.
        // 4) As mentioned above, the invoke() operator needs a Context, so where we can get it from?
        // We're gonna use the already pre-programmed androidXModule. So we're gonna pass in
        // instance() which is retrieved from the androidXModule. And the instance() type in this
        // case will be Context.
        bind() from singleton { HerokuBloggingDatabase(instance()) }
        //NOTE:
        // We want to get the AuthorDao from the HerokuBloggingDatabase (which was previously
        // bounded). That's why we need to get the instance of the HerokuBloggingDatabase from
        // whatever is returned from the above binding. Then from that instance, we want to get
        // the authorDao().
        bind() from singleton { instance<HerokuBloggingDatabase>().authorDao() }
        bind() from singleton { instance<HerokuBloggingDatabase>().postDao() }
        bind() from singleton { instance<HerokuBloggingDatabase>().commentDao() }
        //NOTE:
        // ConnectivityInterceptor has an interface but ALSO an underlying implementation. So we're
        // gonna bind this interface WITH a singleton.
        // And we wanna return ConnectivityInterceptorImpl.
        bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptorImpl(instance()) }
        bind() from singleton { HerokuBloggingApiService(instance()) }
        bind<AuthorNetworkDataSource>() with singleton { AuthorNetworkDataSourceImpl(instance()) }
        bind<PostNetworkDataSource>() with singleton { PostNetworkDataSourceImpl(instance()) }
        bind<CommentNetworkDataSource>() with singleton { CommentNetworkDataSourceImpl(instance()) }
        //NOTE:
        // 1) HerokuBloggingRepository has an interface but ALSO an underlying implementation.
        // So we're gonna bind this interface WITH a singleton.
        // And we wanna return HerokuBloggingRepositoryImpl.
        // 2) HerokuRepositoryImpl needs two instances for every entity: the first is for the DAO
        // and the second is for the Network Data Source.
        bind<HerokuBloggingRepository>() with singleton {
            HerokuBloggingRepositoryImpl(
                instance(),
                instance(),
                instance(),
                instance(),
                instance(),
                instance()
            )
        }
        bind() from provider {
            AuthorListViewModelFactory(
                instance()
            )
        }
        bind() from factory { id: Int ->
            AuthorViewModelFactory(
                instance(),
                id
            )
        }
        bind() from factory { id: Int ->
            PostViewModelFactory(
                instance(),
                id
            )
        }
        bind() from factory { id: Int ->
            CommentListViewModelFactory(
                instance(),
                id
            )
        }
    }
}