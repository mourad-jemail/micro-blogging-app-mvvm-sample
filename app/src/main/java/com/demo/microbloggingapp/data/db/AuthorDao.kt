package com.demo.microbloggingapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.relation.AuthorWithPostList

@Dao
interface AuthorDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(authors: List<Author>)

    @Update
    fun update(author: Author)

    @Query("select * from author order by id asc")
    fun getAuthorList(): LiveData<List<Author>>

    @Query("select count(id) from author")
    fun countAuthors(): Int

    @Query("delete from author")
    fun deleteOldAuthors()

    @Query("SELECT * FROM author where id = :authorId")
    fun getAuthorWithPostList(authorId: Int): LiveData<AuthorWithPostList>
}