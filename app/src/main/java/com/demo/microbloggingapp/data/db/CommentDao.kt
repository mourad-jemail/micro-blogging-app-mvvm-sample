package com.demo.microbloggingapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.demo.microbloggingapp.data.db.entity.Comment

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comments: List<Comment>)

    @Query("select count(id) from comment where postId = :postId")
    fun countComments(postId: Int): Int

    @Query("delete from comment")
    fun deleteOldComments()

    @Query("select * from comment where postId = :postId")
    fun getCommentList(postId: Int): LiveData<List<Comment>>
}