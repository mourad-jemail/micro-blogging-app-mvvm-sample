package com.demo.microbloggingapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.data.db.entity.Post

@Database(
    entities = [Author::class, Post::class, Comment::class],
    version = 1
)
abstract class HerokuBloggingDatabase : RoomDatabase() {
    abstract fun authorDao(): AuthorDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao

    companion object {
        //NOTE:
        // @Volatile: All of the threads have immediate access to the HerokuBloggingDatabase instance.
        @Volatile
        private var instance: HerokuBloggingDatabase? = null

        //NOTE:
        // We use LOCK object because we're gonna be using threads and we need to make sure we don't
        // allow multiple threads doing the same thing or multiple instances at the same time.
        private val LOCK = Any()

        //NOTE:
        // 1- instance ?: means if instance is not null we return it straight away.
        // 2- synchronized(LOCK): the method will be protected from concurrent execution by multiple
        // threads by the monitor of the instance (or, for static methods, the class) on which the
        // method is defined.
        // @See https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.jvm/-synchronized/
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            //NOTE:
            // also is used when we want to send an "it" as an argument without modifying it.
            // In this case, "it" refers to {#buildDatabase(Context)}
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                HerokuBloggingDatabase::class.java,
                "blogging.db"
            )
                .build()
    }
}