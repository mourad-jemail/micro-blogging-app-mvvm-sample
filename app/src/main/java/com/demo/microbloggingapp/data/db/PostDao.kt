package com.demo.microbloggingapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.demo.microbloggingapp.data.db.entity.Post
import com.demo.microbloggingapp.data.db.entity.relation.PostWithCommentList

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Post>)

    @Query("select count(id) from post where authorId = :authorId")
    fun countPosts(authorId: Int): Int

    @Query("delete from post")
    fun deleteOldPosts()

    @Query("select * from post where id = :postId")
    fun getPostWithCommentList(postId: Int): LiveData<PostWithCommentList>
}