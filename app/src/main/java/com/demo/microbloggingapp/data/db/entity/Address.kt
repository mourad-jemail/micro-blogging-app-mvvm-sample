package com.demo.microbloggingapp.data.db.entity

data class Address(
    val latitude: Double,
    val longitude: Double
)