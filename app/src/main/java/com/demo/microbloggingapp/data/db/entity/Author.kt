package com.demo.microbloggingapp.data.db.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "author", indices = [Index(value = ["id"])])
data class Author(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val userName: String,
    val email: String,
    val avatarUrl: String,
    @Embedded(prefix = "address_")
    val address: Address
)