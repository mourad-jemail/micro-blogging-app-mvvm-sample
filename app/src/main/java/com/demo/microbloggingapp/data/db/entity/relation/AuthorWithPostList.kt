package com.demo.microbloggingapp.data.db.entity.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Post

data class AuthorWithPostList(
    @Embedded
    val author: Author,

    @Relation(
        parentColumn = "id",
        entityColumn = "authorId"
    )
    val postList: List<Post> = emptyList()
)