package com.demo.microbloggingapp.data.db.entity.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.data.db.entity.Post

data class PostWithCommentList(
    @Embedded
    val post: Post,

    @Relation(
        parentColumn = "authorId",
        entityColumn = "id"
    )
    val author: Author,

    @Relation(
        parentColumn = "id",
        entityColumn = "postId"
    )
    val commentList: List<Comment> = emptyList()
)