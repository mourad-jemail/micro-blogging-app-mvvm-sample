package com.demo.microbloggingapp.data.network

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor