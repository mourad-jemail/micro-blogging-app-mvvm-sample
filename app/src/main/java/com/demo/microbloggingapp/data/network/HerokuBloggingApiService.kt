package com.demo.microbloggingapp.data.network

import com.demo.microbloggingapp.BuildConfig
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.data.db.entity.Post
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

// https://sym-json-server.herokuapp.com/

interface HerokuBloggingApiService {

    //NOTE:
    // Fetching data from a remote network takes some time. Because of this, we cannot just simply
    // return a List of Authors. That's why we wrapped it inside a Deferred.
    // Deferred is part of Kotlin Coroutines and we can await a Deferred.
//     https://sym-json-server.herokuapp.com/authors
    @GET("authors")
    fun getAuthorList(): Deferred<List<Author>>

//    https://sym-json-server.herokuapp.com/posts?authorId=10
    @GET("posts")
    fun getPostList(
        @Query("authorId") authorId: Int
    ): Deferred<List<Post>>

//    https://sym-json-server.herokuapp.com/comments?postId=12
    @GET("comments")
    fun getCommentList(
        @Query("postId") postId: Int
    ): Deferred<List<Comment>>

    companion object {

        //NOTE:
        // If class implementation was written by adding a simple method ie. create():
        // fun create(): HerokuBloggingApiService {
        //      ...
        // }
        // then, later call this class's method as the following:
        // HerokuBloggingApiService.create()
        // But, when we use invoke() operator to implement the class, we can then call the function
        // simply as follows:
        // HerokuBloggingApiService()
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): HerokuBloggingApiService {

            //NOTE :
            // 1- Interceptor is a functional interface. Since it contains exactly one abstract
            // method, we can omit the name of that method {#intercept(Chain)} when we implement it
            // using a lambda expression.
            // 2- We can call the {#intercept(Chain chain)} method as follows:
            // (Chain chain) -> { ... return ... }
            // Notice the method does not have a name here. This is possible because the interface
            // has only one abstract method, so the compiler can figure out the name.
            // 3- This can be shortened to:
            // (Chain chain) -> ... return ...
            // Notice that here are no curly brackets.
            val requestInterceptor = Interceptor { chain ->

                // NOTE:
                //  @See https://kotlinlang.org/docs/reference/returns.html
                //  Any expression in Kotlin may be marked with a label. Labels have the form of an
                //  identifier followed by the @ sign.
                //  If we need to return from a lambda expression, we have to label it and qualify
                //  the return.
                //  @See https://stackoverflow.com/questions/40160489/kotlin-whats-does-return-mean
                //  In Kotlin, the return@label syntax is used for specifying which function, among
                //  several nested ones, this statement returns from.
                //  Non-labeled return statement return from the nearest (i.e. innermost) enclosing
                //  fun (ignoring lambdas). So in this case, return will finish the execution of
                //  invoke(), not just the lambda.
                //  @See https://kotlinlang.org/docs/reference/inline-functions.html#non-local-returns
                return@Interceptor chain.proceed(chain.request())
            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(connectivityInterceptor)
                .build()

            //NOTE:
            // Since we're wrapping the returned data inside a Deferred in the fetch methods,
            // we need to add a CallAdapterFactory for the Retrofit library where we pass the
            // CoroutineCallAdapterFactory().
            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BuildConfig.API_SERVICE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(HerokuBloggingApiService::class.java)
        }
    }
}