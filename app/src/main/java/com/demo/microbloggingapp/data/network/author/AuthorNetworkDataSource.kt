package com.demo.microbloggingapp.data.network.author

import androidx.lifecycle.LiveData
import com.demo.microbloggingapp.data.db.entity.Author

interface AuthorNetworkDataSource {
    // This interface exposes only LiveData which is not mutable through the following methods.
    // Because we don't want to have outside classes access to changing the LiveData directly
    val downloadedAuthorList: LiveData<List<Author>>

    //NOTE:
    // Suspending functions are at the center of everything coroutines. A suspending function is
    // simply a function that can be paused and resumed at a later time. They can execute a long
    // running operation and wait for it to complete without blocking.
    // The syntax of a suspending function is similar to that of a regular function except for the
    // addition of the suspend keyword. It can take a parameter and have a return type. However,
    // suspending functions can only be invoked by another suspending function or within a coroutine.
    // @See https://stackoverflow.com/questions/47871868/what-does-suspend-function-mean-in-kotlin-coroutine
    suspend fun fetchAuthorList()
}