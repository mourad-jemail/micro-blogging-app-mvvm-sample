package com.demo.microbloggingapp.data.network.author

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demo.microbloggingapp.data.network.HerokuBloggingApiService
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.internal.NoConnectivityException
import java.net.SocketTimeoutException

class AuthorNetworkDataSourceImpl(
    private val apiService: HerokuBloggingApiService
) : AuthorNetworkDataSource {

    // We want to keep our MutableLiveData private because they can be changed
    // So we want to be able to update them only from the inside of the class
    private val _downloadedAuthorList = MutableLiveData<List<Author>>()

    override val downloadedAuthorList: LiveData<List<Author>>
        get() = _downloadedAuthorList

    override suspend fun fetchAuthorList() {
        try {
            val fetchedAuthorList = apiService
                .getAuthorList()
                .await()

            _downloadedAuthorList.postValue(fetchedAuthorList)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection. ", e)
        } catch (e: SocketTimeoutException) {
            Log.e("Timeout", "Socket Timeout Exception. ", e)
        }
    }
}