package com.demo.microbloggingapp.data.network.comment

import androidx.lifecycle.LiveData
import com.demo.microbloggingapp.data.db.entity.Comment

interface CommentNetworkDataSource {
    val downloadedCommentList: LiveData<List<Comment>>

    suspend fun fetchCommentList(postId: Int)
}