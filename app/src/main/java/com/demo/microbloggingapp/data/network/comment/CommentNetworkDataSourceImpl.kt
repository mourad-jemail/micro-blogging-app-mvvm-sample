package com.demo.microbloggingapp.data.network.comment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demo.microbloggingapp.data.network.HerokuBloggingApiService
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.internal.NoConnectivityException
import java.net.SocketTimeoutException

class CommentNetworkDataSourceImpl(
    private val apiService: HerokuBloggingApiService
) : CommentNetworkDataSource {

    private val _downloadedCommentList = MutableLiveData<List<Comment>>()

    override val downloadedCommentList: LiveData<List<Comment>>
        get() = _downloadedCommentList

    override suspend fun fetchCommentList(postId: Int) {
        try {
            val fetchedCommentList = apiService
                .getCommentList(postId)
                .await()

            _downloadedCommentList.postValue(fetchedCommentList)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection. ", e)
        } catch (e: SocketTimeoutException) {
            Log.e("Timeout", "Socket Timeout Exception. ", e)
        }
    }
}