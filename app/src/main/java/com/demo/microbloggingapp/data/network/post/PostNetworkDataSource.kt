package com.demo.microbloggingapp.data.network.post

import androidx.lifecycle.LiveData
import com.demo.microbloggingapp.data.db.entity.Post

interface PostNetworkDataSource {
    val downloadedPostList: LiveData<List<Post>>

    suspend fun fetchPostList(authorId: Int)
}