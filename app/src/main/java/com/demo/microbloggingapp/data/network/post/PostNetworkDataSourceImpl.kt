package com.demo.microbloggingapp.data.network.post

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demo.microbloggingapp.data.network.HerokuBloggingApiService
import com.demo.microbloggingapp.data.db.entity.Post
import com.demo.microbloggingapp.internal.NoConnectivityException
import java.net.SocketTimeoutException

class PostNetworkDataSourceImpl(
    private val apiService: HerokuBloggingApiService
) : PostNetworkDataSource {

    private val _downloadedPostList = MutableLiveData<List<Post>>()

    override val downloadedPostList: LiveData<List<Post>>
        get() = _downloadedPostList

    override suspend fun fetchPostList(authorId: Int) {
        try {
            val fetchedPostList = apiService
                .getPostList(authorId)
                .await()

            _downloadedPostList.postValue(fetchedPostList)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection. ", e)
        } catch (e: SocketTimeoutException) {
            Log.e("Timeout", "Socket Timeout Exception. ", e)
        }
    }
}