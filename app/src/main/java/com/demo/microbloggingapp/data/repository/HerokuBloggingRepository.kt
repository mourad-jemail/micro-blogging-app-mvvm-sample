package com.demo.microbloggingapp.data.repository

import androidx.lifecycle.LiveData
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.data.db.entity.relation.AuthorWithPostList
import com.demo.microbloggingapp.data.db.entity.relation.PostWithCommentList

interface HerokuBloggingRepository {
    suspend fun getAuthorList(): LiveData<List<Author>>

    suspend fun getAuthorWithPostList(authorId: Int): LiveData<AuthorWithPostList>

    suspend fun getPostWithCommentList(postId: Int): LiveData<PostWithCommentList>

    suspend fun getCommentList(postId: Int): LiveData<List<Comment>>
}