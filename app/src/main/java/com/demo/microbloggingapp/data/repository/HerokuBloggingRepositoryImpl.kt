package com.demo.microbloggingapp.data.repository

import androidx.lifecycle.LiveData
import com.demo.microbloggingapp.data.db.AuthorDao
import com.demo.microbloggingapp.data.db.CommentDao
import com.demo.microbloggingapp.data.db.PostDao
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.data.db.entity.Post
import com.demo.microbloggingapp.data.db.entity.relation.AuthorWithPostList
import com.demo.microbloggingapp.data.db.entity.relation.PostWithCommentList
import com.demo.microbloggingapp.data.network.author.AuthorNetworkDataSource
import com.demo.microbloggingapp.data.network.comment.CommentNetworkDataSource
import com.demo.microbloggingapp.data.network.post.PostNetworkDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HerokuBloggingRepositoryImpl(
    private val authorDao: AuthorDao,
    private val authorNetworkDataSource: AuthorNetworkDataSource,
    private val postDao: PostDao,
    private val postNetworkDataSource: PostNetworkDataSource,
    private val commentDao: CommentDao,
    private val commentNetworkDataSource: CommentNetworkDataSource
) : HerokuBloggingRepository {

    init {
        authorNetworkDataSource.downloadedAuthorList.observeForever { newAuthorList ->
            persistFetchedAuthorList(newAuthorList)
        }
        postNetworkDataSource.downloadedPostList.observeForever { newPostList ->
            persistFetchedPostList(newPostList)
        }
        commentNetworkDataSource.downloadedCommentList.observeForever { newCommentList ->
            persistFetchedCommentList(newCommentList)
        }
    }

    override suspend fun getAuthorList(): LiveData<List<Author>> {
        return withContext(Dispatchers.IO) {
            // 1) Fetch data from network and store it inside a LiveData<List<Author>> object.
            authorNetworkDataSource.fetchAuthorList()

            // 2) The observer is then notified (because we've already set it in the init block) and
            // it will store the retrieved data in database owing to the
            // persistFetchedAuthorList(List<Author>) function.

            // 3) After the List<Author> is stored in database, return a LiveData<List<Author>> from
            // database.
            return@withContext authorDao.getAuthorList()
        }
    }

    override suspend fun getAuthorWithPostList(authorId: Int): LiveData<AuthorWithPostList> {
        return withContext(Dispatchers.IO) {
            postNetworkDataSource.fetchPostList(authorId)
            return@withContext authorDao.getAuthorWithPostList(authorId)
        }
    }

    override suspend fun getPostWithCommentList(postId: Int): LiveData<PostWithCommentList> {
        return withContext(Dispatchers.IO) {
            commentNetworkDataSource.fetchCommentList(postId)
            return@withContext postDao.getPostWithCommentList(postId)
        }
    }

    override suspend fun getCommentList(postId: Int): LiveData<List<Comment>> {
        return withContext(Dispatchers.IO) {
            commentNetworkDataSource.fetchCommentList(postId)
            return@withContext commentDao.getCommentList(postId)
        }
    }

    private fun persistFetchedAuthorList(fetchedAuthorList: List<Author>) {
        fun deleteOldAuthorList() {
            authorDao.deleteOldAuthors()
        }

        //NOTE:
        // we can use GlobalScope here because a Repository class is not a lifecycle dependant.
        // launch(..) returns nothing, just a job.
        // Dispatchers.IO will ensure we can spin up a massive amount of little threats at once,
        // and also then destroy them and we aren't gonna get any performance penalties
        GlobalScope.launch(Dispatchers.IO) {
            deleteOldAuthorList()
            authorDao.insert(fetchedAuthorList)
        }
    }

    private fun persistFetchedPostList(fetchedPostList: List<Post>) {
        fun deleteOldPostList() {
            postDao.deleteOldPosts()
        }

        GlobalScope.launch(Dispatchers.IO) {
            deleteOldPostList()
            postDao.insert(fetchedPostList)
        }
    }

    private fun persistFetchedCommentList(fetchedCommentList: List<Comment>) {
        fun deleteOldCommentList() {
            commentDao.deleteOldComments()
        }

        GlobalScope.launch(Dispatchers.IO) {
            deleteOldCommentList()
            commentDao.insert(fetchedCommentList)
        }
    }
}