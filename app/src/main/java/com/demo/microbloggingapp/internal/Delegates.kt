package com.demo.microbloggingapp.internal

import kotlinx.coroutines.*

/**
 * Return a lazy coroutine function.
 *
 * This is a higher-order function which takes a suspending function as a parameter and returns it
 * inside a lazy block.
 */
fun <T> lazyDeferred(block: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> {
    return lazy {
        GlobalScope.async(start = CoroutineStart.LAZY) {
            block.invoke(this)
        }
    }
}