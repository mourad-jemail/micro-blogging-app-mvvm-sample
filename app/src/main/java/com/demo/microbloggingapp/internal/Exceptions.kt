package com.demo.microbloggingapp.internal

import java.io.IOException

class NoConnectivityException: IOException()
class DateNotFoundException: Exception()