package com.demo.microbloggingapp.internal

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

object LocalDateFormatter {

    @ExperimentalStdlibApi
    fun formatStringDate(str: String?) = str?.let {

        // capitalize(Locale.getDefault()) function declaration is experimental and its usage must
        // be marked with the following annotation
        val date = LocalDate.parse(it, DateTimeFormatter.ISO_DATE_TIME)
        date.month.name.toLowerCase(Locale.getDefault())
            .capitalize(Locale.getDefault()) + " " + date.dayOfMonth + ", " + date.year
    }
}