package com.demo.microbloggingapp.internal.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

// NOTE:
//  Glide documentation says:
//  All applications can optionally add a AppGlideModule implementation if the Application is
//  implementing any methods in AppGlideModule or wants to use any integration libraries.
//  The AppGlideModule implementation acts as a signal that allows Glide’s annotation processor to
//  generate a single combined class with with all discovered LibraryGlideModules.
//  @See https://bumptech.github.io/glide/doc/configuration.html#appglidemodule
@GlideModule
class HerokuBloggingGlideModule : AppGlideModule()