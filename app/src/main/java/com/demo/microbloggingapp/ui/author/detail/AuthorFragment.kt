package com.demo.microbloggingapp.ui.author.detail

import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.data.db.entity.Post
import com.demo.microbloggingapp.data.db.entity.relation.AuthorWithPostList
import com.demo.microbloggingapp.internal.glide.GlideApp
import com.demo.microbloggingapp.ui.base.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.author_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.factory
import java.io.IOException
import java.util.*

class AuthorFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private val viewModelFactoryInstanceFactory
            : ((Int) -> AuthorViewModelFactory) by factory()

    private lateinit var viewModel: AuthorViewModel

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private lateinit var authorWithPostList: LiveData<AuthorWithPostList>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.author_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val safeArgs = arguments?.let { AuthorFragmentArgs.fromBundle(it) }
        val id = safeArgs?.authorId

        viewModel = ViewModelProvider(this, viewModelFactoryInstanceFactory(id!!))
            .get(AuthorViewModel::class.java)

        bindUI()
    }

    private fun bindUI() = launch {
        updateActionBarTitle(resources.getString(R.string.author_name))
        updateListCount(resources.getString(R.string.list_count_pending))

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@AuthorFragment.context)
            adapter = groupAdapter
        }

        authorWithPostList = viewModel.authorWithPostList.await()

        authorWithPostList.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer

            group_loading.visibility = View.GONE
            cardView_author.visibility = View.VISIBLE
            cardView_post_list.visibility = View.VISIBLE

            updateActionBarTitle(it.author.name)

            initAuthorDetail(it)

            updateListCount(resources.getString(R.string.list_count, it.postList.size))

            groupAdapter.apply {
                clear()

                if (it.postList.isNotEmpty())
                    addAll(it.postList
                        // Sort post list by date descending
                        .sortedWith(
                            compareByDescending { post -> post.date }
                        ).toPostItems())

                groupAdapter.setOnItemClickListener { item, view ->
                    (item as? PostItem)?.let {
                        showPost(it.post.id, view)
                    }
                }
            }
        })
    }

    private fun updateActionBarTitle(name: String) {
        (activity as? AppCompatActivity)?.supportActionBar?.title = name
    }

    private fun updateListCount(str: String) {
        textView_posts_count.text = str
    }

    private fun initAuthorDetail(authorWithPostList: AuthorWithPostList) {
        authorWithPostList.let {
            GlideApp.with(this@AuthorFragment)
                .load(it.author.avatarUrl)
                .placeholder(R.drawable.ic_satisfied)
                .error(R.drawable.ic_dissatisfied)
                .circleCrop()
                .into(imageView)

            textView_name.text = it.author.name
            textView_user_name.text = it.author.userName
            textView_email.text = it.author.email

            val country = getCountry(it.author)
            if (country != "") {
                textView_address.text = country
                textView_address.visibility = View.VISIBLE
            } else {
                textView_address.visibility = View.INVISIBLE
            }
        }
    }

    private fun getCountry(author: Author): String {
        val geoCoder = Geocoder(context?.applicationContext, Locale.getDefault())
        var addressList: List<android.location.Address>

        author.let {
            try {
                addressList = geoCoder.getFromLocation(
                    it.address.latitude,
                    it.address.longitude,
                    1
                )

                if (!addressList.isNullOrEmpty()) {
                    return addressList[0].countryName ?: ""
                }

                return ""
            } catch (e: IOException) {
                Log.e("Geocoder", "grpc failed. ", e)
            }
        }
        return ""
    }

    private fun List<Post>.toPostItems(): List<PostItem> {
        return this.map {
            PostItem(it)
        }
    }

    private fun showPost(id: Int, view: View) {
        val actionPost = AuthorFragmentDirections.actionPost(id)
        Navigation.findNavController(view).navigate(actionPost)
    }
}