package com.demo.microbloggingapp.ui.author.detail

import androidx.lifecycle.ViewModel
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository
import com.demo.microbloggingapp.internal.lazyDeferred

class AuthorViewModel(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModel() {

    val authorWithPostList by lazyDeferred {
        bloggingRepository.getAuthorWithPostList(id)
    }
}