package com.demo.microbloggingapp.ui.author.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository

class AuthorViewModelFactory(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AuthorViewModel(bloggingRepository, id) as T
    }
}