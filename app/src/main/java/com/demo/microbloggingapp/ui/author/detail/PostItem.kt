package com.demo.microbloggingapp.ui.author.detail

import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Post
import com.demo.microbloggingapp.internal.DateNotFoundException
import com.demo.microbloggingapp.internal.LocalDateFormatter
import com.demo.microbloggingapp.internal.glide.GlideApp
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_post.*

class PostItem(
    val post: Post
) : Item() {

    // stringToDate(String) function declaration is experimental and its usage must be marked with
    // the following annotation
    @ExperimentalStdlibApi
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.apply {
            updateImage()
            textView_title.text = post.title
            textView_body.text = post.body

            val date =
                LocalDateFormatter.formatStringDate(post.date) ?: throw DateNotFoundException()
            textView_date.text = date
        }
    }

    override fun getLayout() = R.layout.item_post

    private fun GroupieViewHolder.updateImage() {
        GlideApp.with(this.containerView)
            .load(post.imageUrl)
            .placeholder(R.drawable.ic_article)
            .error(R.drawable.ic_article_failed)
            .into(imageView)
    }
}