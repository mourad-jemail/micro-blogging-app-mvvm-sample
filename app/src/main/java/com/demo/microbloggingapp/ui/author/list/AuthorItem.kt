package com.demo.microbloggingapp.ui.author.list

import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.internal.glide.GlideApp
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_author.*

class AuthorItem(
    val author: Author
) : Item() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.apply {
            updateAvatar()
            textView_name.text = author.name
            textView_user_name.text = author.userName
            textView_email.text = author.email
        }
    }

    override fun getLayout() = R.layout.item_author

    private fun GroupieViewHolder.updateAvatar() {
        GlideApp.with(this.containerView)
            .load(author.avatarUrl)
            .placeholder(R.drawable.ic_satisfied)
            .error(R.drawable.ic_dissatisfied)
            .circleCrop()
            .into(imageView)
    }
}