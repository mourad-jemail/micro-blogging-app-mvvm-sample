package com.demo.microbloggingapp.ui.author.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Author
import com.demo.microbloggingapp.ui.base.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.author_list_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class AuthorListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: AuthorListViewModelFactory by instance()

    private lateinit var viewModel: AuthorListViewModel

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private lateinit var authorList: LiveData<List<Author>>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.author_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(AuthorListViewModel::class.java)

        bindUI()
    }

    private fun bindUI() = launch {
        updateActionBarTitle()
        updateListCount(resources.getString(R.string.list_count_pending))

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@AuthorListFragment.context)
            adapter = groupAdapter
        }

        authorList = viewModel.authorList.await()

        authorList.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer

            group_loading.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE

            updateListCount(resources.getString(R.string.list_count, it.size))

            groupAdapter.apply {
                clear()

                if (it.isNotEmpty())
                    addAll(it.toAuthorItems())

                setOnItemClickListener { item, view ->
                    (item as? AuthorItem)?.let {
                        showAuthor(it.author.id, view)
                    }
                }
            }
        })
    }

    private fun updateActionBarTitle() {
        (activity as? AppCompatActivity)?.supportActionBar?.title =
            resources.getString(R.string.app_name)
    }

    private fun updateListCount(str: String) {
        textView_authors_count.text = str
    }

    private fun List<Author>.toAuthorItems(): List<AuthorItem> {
        return this.map {
            AuthorItem(it)
        }
    }

    private fun showAuthor(id: Int, view: View) {
        val actionAuthor = AuthorListFragmentDirections.actionAuthor(id)
        Navigation.findNavController(view).navigate(actionAuthor)
    }
}