package com.demo.microbloggingapp.ui.author.list

import androidx.lifecycle.ViewModel
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository
import com.demo.microbloggingapp.internal.lazyDeferred

class AuthorListViewModel(
    private val herokuBloggingRepository: HerokuBloggingRepository
) : ViewModel() {

    // NOTE:
    //  According to Android developers guides by Google, one of the benefits of using the ViewModel
    //  class is to allow data to survive configuration changes.
    //  To leverage that advantage, we'll use a by lazy block in order to fetch data only upon first
    //  time the ViewModel is instantiated (by the View).
    //  Therefore if the View is re-created, it receives the same AuthorListViewModel instance
    //  created the first time. And most importantly, retained authorList is immediately available
    //  to the next View instance.
    val authorList by lazyDeferred {
        // NOTE:
        //  Since, we cannot call suspend functions inside a simple lazy function, we replaced
        //  lazy function with our lazyDeferred function we created previously.
        //  lazyDeferred takes a suspending function as a parameter and returns it in a lazy block.
        herokuBloggingRepository.getAuthorList()
    }
}