package com.demo.microbloggingapp.ui.author.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository

// NOTE:
//  A ViewModel class is all about preserving state and is not destroyed on configuration
//  change (for example: when the View is destroyed and re-created on screen rotation).
//  If it's directly instantiated inside the View, it will be re-instantiated every time the View
//  is re-created. But we cannot just re-instantiate the ViewModel every time the View is re-created.
//  We only want to create a new instance of the ViewModel upon the first launch of the View.
//  Then, on subsequent launches of the View, we only want to get the already present instance of
//  the ViewModel, because it preserves the state inside it.
//  All of this is possible through the ViewModelProvider. Its job is to preserve the ViewModel.
//  We only need to pass the ViewModelFactory into it.
class AuthorListViewModelFactory(
    private val bloggingRepository: HerokuBloggingRepository
) : ViewModelProvider.NewInstanceFactory() {

    /**
     * Create a new instance of the AuthorListViewModel class.
     */
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AuthorListViewModel(bloggingRepository) as T
    }
}