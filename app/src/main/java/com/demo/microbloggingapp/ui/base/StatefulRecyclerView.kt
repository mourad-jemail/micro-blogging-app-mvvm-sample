package com.demo.microbloggingapp.ui.base

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

const val SAVED_SUPER_STATE = "super-state"
const val SAVED_LAYOUT_MANAGER = "layout-manager-state"

/**
 * Class [StatefulRecyclerView] extends [RecyclerView] and adds position management on configuration changes.
 * @link https://gist.github.com/FrantisekGazo/a9cc4e18cee42199a287
 * https://panavtec.me/retain-restore-recycler-view-scroll-position
 * https://medium.com/androiddevelopers/restore-recyclerview-scroll-position-a8fbdc9a9334
 * 
 *
 * @author FrantisekGazo
 * @version 2016-03-15
 */
class StatefulRecyclerView : RecyclerView {

    private var mLayoutManagerSavedState: Parcelable? = null

    constructor(context: Context?) : super(context!!)

    constructor(
        context: Context?,
        attrs: AttributeSet?
    ) : super(context!!, attrs)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context!!, attrs, defStyle)

    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SAVED_SUPER_STATE, super.onSaveInstanceState())
        bundle.putParcelable(SAVED_LAYOUT_MANAGER, this.layoutManager!!.onSaveInstanceState())
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        var state: Parcelable? = state
        if (state is Bundle) {
            val bundle = state
            mLayoutManagerSavedState = bundle.getParcelable(SAVED_LAYOUT_MANAGER)
            state = bundle.getParcelable(SAVED_SUPER_STATE)
        }
        super.onRestoreInstanceState(state)
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        restorePosition()
    }

    /**
     * Restores scroll position after configuration change.
     *
     *
     * **NOTE:** Must be called after adapter has been set.
     */
    private fun restorePosition() {
        if (mLayoutManagerSavedState != null) {
            this.layoutManager!!.onRestoreInstanceState(mLayoutManagerSavedState)
            mLayoutManagerSavedState = null
        }
    }
}