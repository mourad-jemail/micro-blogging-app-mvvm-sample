package com.demo.microbloggingapp.ui.comment

import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.internal.DateNotFoundException
import com.demo.microbloggingapp.internal.LocalDateFormatter
import com.demo.microbloggingapp.internal.glide.GlideApp
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_comment.*

class CommentItem(
    val comment: Comment
) : Item() {

    // formatStringDate(String) function declaration is experimental and its usage must be annotated
    // with the following annotation
    @ExperimentalStdlibApi
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.apply {
            updateImage()
            textView_user_name.text = comment.userName
            textView_user_email.text = comment.email

            val date =
                LocalDateFormatter.formatStringDate(comment.date) ?: throw DateNotFoundException()
            textView_comment_date.text = date

            textView_comment_body.text = comment.body
        }
    }

    override fun getLayout() = R.layout.item_comment

    private fun GroupieViewHolder.updateImage() {
        GlideApp.with(this.containerView)
            .load(comment.avatarUrl)
            .placeholder(R.drawable.ic_satisfied)
            .error(R.drawable.ic_dissatisfied)
            .circleCrop()
            .into(imageView_user)
    }
}