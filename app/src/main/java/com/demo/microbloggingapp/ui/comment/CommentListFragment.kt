package com.demo.microbloggingapp.ui.comment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.Comment
import com.demo.microbloggingapp.ui.base.ScopedFragment
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.comment_list_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.factory

class CommentListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private val viewModelFactoryInstanceFactory
            : ((Int) -> CommentListViewModelFactory) by factory()

    private lateinit var viewModel: CommentListViewModel

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()
    private lateinit var commentList: LiveData<List<Comment>>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.comment_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val safeArgs = arguments?.let { CommentListFragmentArgs.fromBundle(it) }
        val id = safeArgs?.postId

        viewModel = ViewModelProvider(this, viewModelFactoryInstanceFactory(id!!))
            .get(CommentListViewModel::class.java)

        bindUI()
    }

    private fun bindUI() = launch {
        updateActionBarTitle()
        updateListCount(resources.getString(R.string.list_count_pending))

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@CommentListFragment.context)
            adapter = groupAdapter
        }

        commentList = viewModel.commentList.await()

        commentList.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer

            group_loading.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE

            updateListCount(resources.getString(R.string.list_count, it.size))

            groupAdapter.apply {
                clear()

                if (it.isNotEmpty())
                    addAll(it
                        // Sort comment list by date descending
                        .sortedWith(
                            compareByDescending { comment -> comment.date }
                        ).toCommentItems())
            }
        })
    }

    private fun updateActionBarTitle() {
        (activity as? AppCompatActivity)?.supportActionBar?.title =
            resources.getString(R.string.comment_list_header)
    }

    private fun updateListCount(str: String) {
        textView_comments_count.text = str
    }

    private fun List<Comment>.toCommentItems(): List<CommentItem> {
        return this.map {
            CommentItem(it)
        }
    }
}