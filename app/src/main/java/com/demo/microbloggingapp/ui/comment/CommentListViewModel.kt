package com.demo.microbloggingapp.ui.comment

import androidx.lifecycle.ViewModel
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository
import com.demo.microbloggingapp.internal.lazyDeferred

class CommentListViewModel(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModel() {

    val commentList by lazyDeferred {
        bloggingRepository.getCommentList(id)
    }
}