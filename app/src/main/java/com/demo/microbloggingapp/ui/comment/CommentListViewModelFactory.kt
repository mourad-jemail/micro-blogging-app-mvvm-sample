package com.demo.microbloggingapp.ui.comment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository

class CommentListViewModelFactory(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CommentListViewModel(bloggingRepository, id) as T
    }
}