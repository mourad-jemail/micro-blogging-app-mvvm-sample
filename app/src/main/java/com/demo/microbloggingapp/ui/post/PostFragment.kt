package com.demo.microbloggingapp.ui.post

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.demo.microbloggingapp.R
import com.demo.microbloggingapp.data.db.entity.relation.PostWithCommentList
import com.demo.microbloggingapp.internal.DateNotFoundException
import com.demo.microbloggingapp.internal.LocalDateFormatter
import com.demo.microbloggingapp.internal.glide.GlideApp
import com.demo.microbloggingapp.ui.base.ScopedFragment
import kotlinx.android.synthetic.main.post_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.factory

class PostFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()

    private val viewModelFactoryInstanceFactory
            : ((Int) -> PostViewModelFactory) by factory()

    private lateinit var viewModel: PostViewModel

    private lateinit var postWithCommentList: LiveData<PostWithCommentList>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_fragment, container, false)
    }

    @ExperimentalStdlibApi
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val safeArgs = arguments?.let { PostFragmentArgs.fromBundle(it) }
        val id = safeArgs?.postId

        viewModel = ViewModelProvider(this, viewModelFactoryInstanceFactory(id!!))
            .get(PostViewModel::class.java)

        bindUI()
    }

    @ExperimentalStdlibApi
    private fun bindUI() = launch {
        updateActionBarTitle(resources.getString(R.string.post_title))
        updateListCount(resources.getString(R.string.list_count_pending))

        postWithCommentList = viewModel.postWithCommentList.await()

        postWithCommentList.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer

            group_loading.visibility = View.GONE
            nestedScrollView_post.visibility = View.VISIBLE

            updateActionBarTitle(it.post.title)

            initPostDetail(it)

            updateListCount(resources.getString(R.string.comment_list_count, it.commentList.size))

            textView_comments_count.setOnClickListener { view ->
                showCommentList(it.post.id, view)
            }
        })
    }

    private fun updateActionBarTitle(title: String) {
        (activity as? AppCompatActivity)?.supportActionBar?.title = title
    }

    private fun updateListCount(str: String) {
        textView_comments_count.text = str
    }

    @ExperimentalStdlibApi
    private fun initPostDetail(postWithCommentList: PostWithCommentList) {
        postWithCommentList.let {
            GlideApp.with(this@PostFragment)
                .load(it.post.imageUrl)
                .placeholder(R.drawable.ic_article)
                .listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar_image_loading.visibility = View.GONE
                        imageView.visibility = View.VISIBLE
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar_image_loading.visibility = View.GONE
                        imageView_error.visibility = View.VISIBLE
                        return false
                    }
                })
                .into(imageView)

            textView_title.text = it.post.title
            textView_body.text = it.post.body

            val date =
                LocalDateFormatter.formatStringDate(it.post.date) ?: throw DateNotFoundException()
            textView_post_date.text = date

            GlideApp.with(this@PostFragment)
                .load(it.author.avatarUrl)
                .placeholder(R.drawable.ic_satisfied)
                .error(R.drawable.ic_dissatisfied)
                .circleCrop()
                .into(imageView_author)
            textView_author_name.text = it.author.name
        }
    }

    private fun showCommentList(id: Int, view: View) {
        val actionPost = PostFragmentDirections.actionPostFragmentToCommentListFragment(id)
        Navigation.findNavController(view).navigate(actionPost)
    }
}