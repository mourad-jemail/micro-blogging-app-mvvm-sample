package com.demo.microbloggingapp.ui.post

import androidx.lifecycle.ViewModel
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository
import com.demo.microbloggingapp.internal.lazyDeferred

class PostViewModel(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModel() {

    val postWithCommentList by lazyDeferred {
        bloggingRepository.getPostWithCommentList(id)
    }
}