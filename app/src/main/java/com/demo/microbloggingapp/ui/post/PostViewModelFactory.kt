package com.demo.microbloggingapp.ui.post

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.microbloggingapp.data.repository.HerokuBloggingRepository

class PostViewModelFactory(
    private val bloggingRepository: HerokuBloggingRepository,
    private val id: Int
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostViewModel(bloggingRepository, id) as T
    }
}